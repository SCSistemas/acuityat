describe('Wrong Data Login Test Suite', function() {
    it('UI3 Enabled', function() {
        browser.get("http://trade-qa.acuityads.com:8080//login.html#/");
        browser.driver.manage().window().maximize();
        expect(browser.getTitle()).toEqual("AcuityAds | Login");
    });

    it('Login - Wrong Password ', function() {
        element(by.name('userName')).clear().sendKeys('AdminQA');
        element(by.name('password')).clear().sendKeys('sdasdasd');
        var el = element(by.className('btn btn-info btn-block aadev-submit ng-binding')).click();
        expect(browser.getTitle()).not.toEqual("AcuityAds | Programmatic Marketing Platform");
    });

    it('Login - Wrong Username', function() {
        element(by.name('userName')).clear().sendKeys('wererwerr');
        element(by.name('password')).clear().sendKeys('AdminQA');
        var el = element(by.className('btn btn-info btn-block aadev-submit ng-binding')).click();
        expect(browser.getTitle()).not.toEqual("AcuityAds | Programmatic Marketing Platform");
    });

    it('Login - Wrong Username/Password', function() {
        element(by.name('userName')).clear().sendKeys('xxxxasdasd23213xxxx');
        element(by.name('password')).clear().sendKeys('xxxxx12312312xxxx');
        var el = element(by.className('btn btn-info btn-block aadev-submit ng-binding')).click();
        expect(browser.getTitle()).not.toEqual("AcuityAds | Programmatic Marketing Platform");
    });
});
