var request = require("superagent");
describe('API Licensee Tests', function () {
    it('API New Licensee', function () {

        var data = '{"userName": "AdminQA", "password": "AdminQA"}';

        return request.post('http://trade-qa.acuityads.com:8080/aa/api/v1/auth/login')
                      .set('Content-Type', 'application/json')
                      .send(data).then(function success(resp) {
            console.log(" status Login = ", resp.status);
            console.log(" authenticationToken = ", resp.body.data.authenticationToken);

            var data2 = '{"status":"A","address":{"address1":"","address2":"","city":"","countryCode":"","postalCode":"","regionCode":""},' +
                '"phone":{"areaCode":"","countryCode":"","extension":"","phoneNumber":""},' +
                '"fax":{"countryCode":"","areaCode":"","phoneNumber":"","extension":""},' +
                '"salesRepId":155,' +
                '"status":"A",' +
                '"website":"",' +
                '"name":"",' +
                '"displayName":"",' +
                '"licenseeId":1,' +
                '"campaignManagerId":155}';

            return request.post('http://trade-qa.acuityads.com:8080/aa/api/v1/licensee/')
                .set('X-Acuity-Authentication-Token', resp.body.data.authenticationToken)
                .set('Content-Type', 'application/json')
                .send(data2).then(function success(resp2) {
                    console.log(" status Add Licensee = ", resp2.status);
                    var bool =  (resp2.status == 200 &&  resp2.body.data.agencyId != undefined);
                    return expect(false).toEqual(bool);
                }, function error(reason2) {
                    console.error(" status Add Licensee = ", reason2.status);
                    console.error(" code Add Licensee = ", reason2.response.body.errors[0]);
                    return expect('Size').toEqual(reason2.response.body.errors[0].code);
                });
        }, function error(reason) {
            console.error(" status Login = ", reason.status);
            console.error(" errors Login = ", reason.response.body.errors);
            return expect(true).toEqual(reason.response.body.errors);
        });


    });
});
