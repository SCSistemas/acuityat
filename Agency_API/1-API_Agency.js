var request = require("superagent");
describe('API Licensee Tests', function () {
    it('API New Licensee', function () {

        var data = '{"userName": "Darwin", "password": "Darwin"}';

        return request.post('http://trade-qa.acuityads.com:8080/aa/api/v1/auth/login')
                      .set('Content-Type', 'application/json')
                      .send(data).then(function success(resp) {
            console.log(" status Login = ", resp.status);
            console.log(" authenticationToken = ", resp.body.data.authenticationToken);

            var data2 = '{"status":"A","address":{"address1":"181 Bay Street","address2":"222222","city":"Toronto","countryCode":"CA","postalCode":"ON","regionCode":"ON"},' +
                '"phone":{"areaCode":"","countryCode":"","extension":"","phoneNumber":"8888888888"},' +
                '"fax":{"countryCode":"","areaCode":"","phoneNumber":"7777777777","extension":""},' +
                '"salesRepId":155,' +
                '"status":"A",' +
                '"website":"http://www.lic.com",' +
                '"name":"Lic test ad",' +
                '"displayName":"Ag test ad",' +
                '"licenseeId":97,' +
                '"campaignManagerId":155}';

            return request.post('http://trade-qa.acuityads.com:8080/aa/api/v1/agency/')
                .set('X-Acuity-Authentication-Token', resp.body.data.authenticationToken)
                .set('Content-Type', 'application/json')
                .send(data2).then(function success(resp2) {
                    console.log(" status Add Agency = ", resp2.status);
                    console.log(" Agency ID = ", resp2.body.data.agencyId);
                    var bool =  (resp2.status == 200 &&  resp2.body.data.agencyId != undefined);
                    return expect(true).toEqual(true);
                }, function error(reason2) {
                    console.error(" status Add Licensee = ", reason2.status);
                    console.error(" errors Add Licensee = ", reason2.response.body.errors);
                    return expect(true).toEqual(reason2.response.body.errors);
                });
        }, function error(reason) {
            console.error(" status Login = ", reason.status);
            console.error(" errors Login = ", reason.response.body.errors);
            return expect(true).toEqual(reason.response.body.errors);
        });


    });
});
