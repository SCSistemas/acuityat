describe('New Campaign Test', function() {

    beforeAll(function () {
        browser.get("http://trade-qa.acuityads.com:8080//login.html");
        browser.driver.manage().window().maximize();
        expect(browser.getTitle()).toEqual("AcuityAds | Login");
        element(by.name('userName')).clear().sendKeys('AdminQA');
        element(by.name('password')).clear().sendKeys('AdminQA');
        element(by.className('btn btn-info btn-block aadev-submit ng-binding')).click();
        expect(browser.getTitle()).toEqual("AcuityAds | Programmatic Marketing Platform");
    });

    /*afterEach(function() {
        element(by.name('name')).clear();                               //Licensee Name
        element(by.name('displayName')).clear();                        //Display Name
        element(by.name('website')).clear();                            //Website
        element(by.name('address1')).clear();                           //Address 181 Bay Street, Toronto, ON M5J 2T3, CA
        element(by.name('address2')).clear();                           //Suite
        element(by.name('phone')).clear();                              //Phone
        element(by.name('fax')).clear();                                //Fax
    });*/

    it('New Campaign - Creation Test', function() {
        var EC = protractor.ExpectedConditions;
        var Camp = element(by.xpath('//span[text()="Campaigns"]')); //Menu Campaigns
        browser.wait(EC.presenceOf(Camp), 5000);
        Camp.click();
        //browser.sleep(5000);
        var newCamp = element(by.xpath('//*[@id="side-menu"]/li[3]/ul/li[5]/a')); //Menu New Campaing
        browser.wait(EC.visibilityOf(newCamp), 5000);
        newCamp.click();
        var campName = element(by.name('masterCampaignName'));
        browser.wait(EC.visibilityOf(campName), 5000);
        campName.sendKeys('Campaign_Test'); //Nombre de la campana a tomar de excel

        element(by.name('advertiserName')).click();

        // Select Advertiser Modal
        var EC = protractor.ExpectedConditions;
        var el1 = element.all(by.css('[ng-click="$select.activate()"]'));

        var email = element(by.xpath('//*[@id="page-top"]/div[5]/div/div/div/form/div[2]/div/div[1]/div/div/div/span/span[1]'));
        browser.wait(EC.visibilityOf(email), 5000);
        email.click();

        element(by.xpath('//span[contains(text(), "DP_Licensee (450)")]')).click();

        var email = element(by.xpath('//*[@id="page-top"]/div[5]/div/div/div/form/div[2]/div/div[2]/div/div/div/span/span[1]'));
        browser.wait(EC.visibilityOf(email), 5000);
        email.click();

        element(by.xpath('//span[contains(text(), "DP_Agency (248)")]')).click();

        var email = element(by.xpath('//*[@id="page-top"]/div[5]/div/div/div/form/div[2]/div/div[3]/div/div/div/span/span[1]'));
        browser.wait(EC.visibilityOf(email), 5000);
        email.click();

        element(by.xpath('//span[contains(text(), "Test ABP (273)")]')).click();

        element(by.xpath('//*[@id="page-top"]/div[5]/div/div/div/form/div[3]/button[2]')).click();

        element(by.name('campaignCreativeType')).click();
        var creativeType = element(by.xpath('//option[text()="Display"]'));
        browser.wait(EC.visibilityOf(creativeType), 5000);
        creativeType.click();

        element(by.xpath('//*[@id="page-wrapper"]/div/form/div/div[3]/div[2]/div[1]/div[4]/div/div/div[1]/div/input')).click();

        var campCAt = element(by.xpath('//span[contains(text(), "3-D Graphics")]'));
        browser.wait(EC.visibilityOf(campCAt), 5000);
        campCAt.click();

        element(by.name('externalId')).sendKeys('123456');

        element(by.name('numLandingPageLabel')).click();

        //browser.wait(5000);

        var landPage = element(by.xpath('//*[@id="landingPageTb"]/thead/tr[1]/th/button'));
        browser.wait(EC.visibilityOf(landPage), 5000);
        landPage.click();
        landPage.click();

        element(by.name('0')).sendKeys('http://www.google.co.ve');

        element(by.xpath('//button[text()="Save changes"]')).click();



        browser.pause();

        /*element.all(by.repeater('item in $select.items')).isDisplayed().each(function(elem,index) {
            elem.getText().then(function (text) {
                if(text=="DP_Licensee (450)")
                {
                    console.log(text);
                    var option = element.all(by.repeater('item in $select.items')).get(index);
                    browser.wait(EC.visibilityOf(option), 15000);
                    option.click();
                }
            });
        });*/

        /*element.all(by.repeater('item in $select.items')).count().then(function(count) {
            console.log(count);

            for (var index = 0; index < count; index++) {
                var option = element.all(by.repeater('item in $select.items')).get(index);
                option.getText().then(function (text) {
                    if(text=="DP_Licensee (450)")
                        {
                        //console.log(text);
                        //console.log(index);
                        //var option = element.all(by.repeater('item in $select.items')).get(index);
                        //browser.wait(EC.visibilityOf(option), 15000);
                        option.click();
                        };
                });
            };

        });

        browser.pause();*/


        /*var alt = element.all(by.repeater('item in aLicensees'));
        browser.wait(EC.visibilityOf(alt), 5000);*/

        /*element.all(by.xpath('//*[@id="page-top"]/div[5]/div/div/div/form/div[2]/div/div[1]/div/div/ul')).isDisplayed().each(function(element,index) {

                element.getText().then(function (text) {
                    if(text=="DP_Licensee (450)")
                    {
                        element.click();
                    }
                });

        });

        var email2 = element(by.xpath('//*[@id="page-top"]/div[5]/div/div/div/form/div[2]/div/div[2]/div/div/div/span/span[2]'));
        browser.wait(EC.visibilityOf(email2), 5000);
        email2.click();

        element.all(by.repeater('item in Agencies')).isDisplayed().each(function(element,index) {

            element.getText().then(function (text) {
                if(text=="DP_Agency (247)")
                {
                    element.click();
                }
            });

        });*/

        /*element.all(by.xpath('//*[@id="page-top"]/div[5]/div/div/div/form/div[2]/div/div[1]/div/div/ul')).isDisplayed().each(function (elem) {
            elem.getText().then(function (text) {

                if(text!="TAD_Licensee_111 (428)")
                {
                    console.log(text);
                }
            });
        });*/

        /*element(by.xpath('//*[@id="page-top"]/div[5]/div/div/div/form/div[2]/div/div[1]/div/div/ul')).getAttribute('value').then(function(text) {
            expect(text).toBe('Text1');
        });*/

        //  browser.pause(15000);
        //browser.wait(EC.presenceOf(el1), 15000);

        //el1.click();
        //browser.wait(EC.presenceOf(el1), 15000);
        //el1.get(1).sendKeys('QA Licensee');
        //var variable1 = "QA Licensee";
       // var advModal = element(by.xpath('//span[contains(text(), '+variable1+' )]'));
        //advModal.click();

        /*var el2 = element(by.xpath('//*[@id="page-top"]/div[5]/div/div/div/form/div[2]/div/div[2]/div/div/div/span/span[1]'));
        el2.sendKeys('QA Agency');
        var variable2 = "QA Agency";
        var advModal = element(by.xpath('//span[contains(text(), '+variable2+' )]'));
        advModal.click();

        var el3 = element(by.xpath('//*[@id="page-top"]/div[5]/div/div/div/form/div[2]/div/div[3]/div/div/div/span/span[1]'));
        el3.click();
        var variable3 = "QA Advertiser";
        var advModal = element(by.xpath('//span[contains(text(), '+variable3+' )]'));
        advModal.click();*/

    });








});
