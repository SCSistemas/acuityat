describe('New Advertiser Test', function() {

    beforeAll(function () {
        browser.get("http://trade-qa.acuityads.com:8080//login.html");
        browser.driver.manage().window().maximize();
        expect(browser.getTitle()).toEqual("AcuityAds | Login");
        element(by.name('userName')).clear().sendKeys('Darwin');
        element(by.name('password')).clear().sendKeys('Darwin');
        element(by.className('btn btn-info btn-block aadev-submit ng-binding')).click();
        expect(browser.getTitle()).toEqual("AcuityAds | Programmatic Marketing Platform");
    });

    it('New Advertiser - Enter New Advertiser Menu', function() {
        var EC = protractor.ExpectedConditions;
        var Admin = element(by.xpath('.//*[text()="Admin"]'));
        browser.wait(EC.visibilityOf(Admin), 10000);
        Admin.click();
        browser.sleep(5500);
        var Adv = element.all(by.xpath('.//*[text()="Advertisers"]')).get(0);
        //element.all(by.css('.dfdf')).get(0);
        browser.wait(EC.visibilityOf(Adv), 10000);
        Adv.click();
        var NewAdv = element(by.xpath('//*[@id="side-menu"]/li[5]/ul/li[3]/ul/li[3]'));
        browser.wait(EC.visibilityOf(NewAdv), 10000);
        NewAdv.click();
    });

    it('New Advertiser', function() {

        var EC = protractor.ExpectedConditions;
        var LId = element(by.name('licenseeId'));
        browser.wait(EC.visibilityOf(LId), 10000);
        LId.click();

        /*element(by.name('licenseeId')).click();         */                            //Type
        element(by.xpath('.//*[@label="QA Licensee"]')).click();

        element(by.name('agencyId')).click();                                       //Type
        element(by.xpath('.//*[@label="QA Agency"]')).click();


        element(by.name('name')).click().sendKeys('TAD_Advertiser_'+ browser.params.TestSuiteID);
        element(by.name('displayName')).click().sendKeys('TAD_Advertiser_'+ browser.params.TestSuiteID);

        element(by.name('advertiserIndustryId')).click();                           //Campaign Manager
        element(by.xpath('.//option[@label="Computing Products"]')).click();

        element(by.name('advertiserTypeId')).click();                               //Campaign Manager
        element(by.xpath('.//option[@label="Direct"]')).click();

        element(by.name('website')).click().sendKeys('www.google.co.ve');

        element(by.name('advertiserCampaignManagerId')).click();                    //Campaign Manager
        element(by.xpath('.//*[@label="Acuity  CM"]')).click();

        element(by.name('advertiserSalesRepId')).click();                           //Sales Rep
        element(by.xpath('.//option[@label="Acuity Sales"]')).click();

        var addr = element(by.name('address1')).sendKeys('181 Bay Street, Toronto, ON M5J 2T3, Canada');                         //Address 181 Bay Street, Toronto, ON M5J 2T3, CA
        addr.click();
        browser.sleep(1000);
        browser.driver.actions().mouseMove(addr);
        addr.sendKeys(protractor.Key.ARROW_DOWN);
        addr.sendKeys(protractor.Key.TAB);

        element(by.name('address2')).sendKeys('3333');                              //Suite
        element(by.name('phone')).sendKeys('55544433322');                          //Phone
        element(by.name('fax')).sendKeys('5554458956');                             //Fax
    });
});
