describe('Parameters Validation in Licensee Creation Test', function() {

    beforeAll(function () {
        browser.get("http://trade-qa.acuityads.com:8080//login.html");
        browser.driver.manage().window().maximize();
        expect(browser.getTitle()).toEqual("AcuityAds | Login");
        element(by.name('userName')).clear().sendKeys('Darwin');
        element(by.name('password')).clear().sendKeys('Darwin');
        element(by.className('btn btn-info btn-block aadev-submit ng-binding')).click();
        expect(browser.getTitle()).toEqual("AcuityAds | Programmatic Marketing Platform");
    });

    afterEach(function() {
        element(by.name('agencyDisplayName')).clear();                          //Display Name
        element(by.name('website')).clear();                                    //Website
        element(by.name('agencyAddress1')).clear();                             //Address 181 Bay Street, Toronto, ON M5J 2T3, CA
        element(by.name('agencyAddress2')).clear();                             //Suite
        element(by.name('agencyPhone')).clear();                                //Phone
        element(by.name('agencyFax')).clear();                                  //Fax
    });

    it('New Agency - Enter New Agency Menu', function() {
        var EC = protractor.ExpectedConditions;
        var Admin = element(by.xpath('//*[text()="Admin"]'));
        browser.wait(EC.presenceOf(Admin), 15000);
        Admin.click();
        browser.sleep(15000);
        var Lic = element(by.xpath('//*[text()="Agencies"]'));
        browser.wait(EC.visibilityOf(Lic), 15000);
        Lic.click();
        var NewLic = element(by.xpath('//*[@id="side-menu"]/li[5]/ul/li[2]/ul/li[3]'));
        browser.wait(EC.visibilityOf(NewLic), 15000);
        NewLic.click();
    });

    it('New Agency - Required Field Test', function() {
        element(by.name('licenseeId')).click();                                 //Licensee Name
        element(by.name('agencyName')).click();                                 //Display Name
        element(by.name('agencyDisplayName')).click();                          //Type
        element(by.name('website')).click();                                    //Website
        element(by.name('agencyCampaignManagerId')).click();                    //Campaign Manager
        element(by.name('agencySalesRepId')).click();                           //Sales Rep
        element(by.name('agencyAddress1')).click();                             //Address
        element(by.name('agencyAddress2')).click();                             //Suite
        element(by.name('agencyPhone')).click();                                //Phone
        element(by.name('agencyFax')).click();                                  //Fax

        /*---------------------------------------------------------------------------------------------*/

        var requiredFieldMessages = element.all(by.xpath('//span[contains(text(), "This field is required")]'));
        expect(requiredFieldMessages.count()).toEqual(5);
    });

    /*it('New Licensee - Website Format Test', function() {
        var web = element(by.name('website'));
        web.sendKeys('google');                                                                                //Website
        var domainFormat = element(by.xpath('//span[contains(text(), "Expected hostname")]'));
        var button = element(by.xpath('//*[@id="page-wrapper"]/div[1]/div/div[2]/form/div/div[2]/button'));
        expect(button.isEnabled()).toBe(true);
    });*/

    it('New Licensee - Phone Number Lenght Test', function() {
        var phone = element(by.name('agencyPhone')).sendKeys('123456789');
        var phoneFormat = element(by.xpath('//span[contains(text(), "This field is too short")]'));
        expect(phoneFormat.getText()).toEqual("This field is too short");
    });

    it('New Licensee - Adress Lenght Test', function() {
        var Addr = element(by.name('agencyAddress1')).sendKeys('d');
        var Addr = element(by.xpath('//span[contains(text(), "This field is too short")]'));
        expect(Addr.getText()).toEqual("This field is too short");
    });
});