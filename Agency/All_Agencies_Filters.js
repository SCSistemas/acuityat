var filtModal;
var textLic;
var search2;

describe('New Agency Test', function()
{
    beforeAll(function () {
        browser.get("http://trade-qa.acuityads.com:8080//login.html");
        browser.driver.manage().window().maximize();
        expect(browser.getTitle()).toEqual("AcuityAds | Login");
        element(by.name('userName')).clear().sendKeys('AdminQA');
        element(by.name('password')).clear().sendKeys('AdminQA');
        element(by.className('btn btn-info btn-block aadev-submit ng-binding')).click();
        expect(browser.getTitle()).toEqual("AcuityAds | Programmatic Marketing Platform");
    });

    /*afterEach(function () {
        element(by.css('[ng-click="clearFilters()"]')).isDisplayed().then(function (result) {
            element(by.css('[ng-click="clearFilters()"]')).click();
        });

        element(by.css('[ng-click="applyFilters()"]')).isDisplayed().then(function(result) {
            if ( result ) {
                element(by.css('[ng-click="applyFilters()"]')).click();
                //browser.pause();
            } else {
                //browser.pause();
            }
        });
    });*/

    it('New Agencie - Enter New Agency Menu', function() {
        var EC = protractor.ExpectedConditions;
        var Admin = element(by.xpath('.//*[text()="Admin"]'));
        browser.wait(EC.visibilityOf(Admin), 15000);
        Admin.click();
        browser.sleep(15000);
        var Agn = element(by.xpath('.//*[text()="Agencies"]'));
        browser.wait(EC.visibilityOf(Agn), 15000);
        Agn.click();
        var NewAgn = element(by.xpath('//*[@id="side-menu"]/li[5]/ul/li[2]/ul/li[1]'));
        browser.wait(EC.visibilityOf(NewAgn), 15000);
        NewAgn.click();
    });

    it('Check Filters Availability', function() {
        var EC = protractor.ExpectedConditions;
        var Admin2 = element(by.xpath('//Button[contains(text(), "Add a filter")]'));
        browser.wait(EC.presenceOf(Admin2), 5000);
        var filt = element.all(by.repeater('option in aSearchOptions'));
        expect(filt.count()).toEqual(6);
    });

    it('Check Filters Apply', function() {
        var EC = protractor.ExpectedConditions;
        var Admin2 = element(by.xpath('//Button[contains(text(), "Add a filter")]'));
        browser.wait(EC.presenceOf(Admin2), 15000);
        Admin2.click();
        var filt = element.all(by.repeater('option in aSearchOptions'));

        element.all(by.repeater('option in aSearchOptions')).isDisplayed().then(function(result) {
            if ( result )
            {
                result.forEach(function (ele,i)
                {
                    filt.get(i).click();
                    filtModal = element(by.css('[ng-click="$select.activate()"]'));
                    filtModal.click();
                    //filtModal.sendKeys(search2);
                    browser.driver.actions().mouseMove(filtModal);
                    filtModal.sendKeys(protractor.Key.ARROW_DOWN);
                    filtModal.sendKeys(protractor.Key.TAB);
                    filtModal.getText().then(function (text2)
                    {
                        textLic = text2;
                    });

                    element(by.css('[ng-click="applyFilters()"]')).isDisplayed().then(function(result) {
                        if ( result ) {

                            var addFilt2 = element(by.css('[ng-click="addFilters()"]'));
                            //browser.wait(EC.presenceOf(addFilt2), 5000);
                            addFilt2.click();
                            //browser.pause();
                        } else {
                            //browser.pause();
                        }
                    });
                    element(by.css('[ng-click="applyFilters()"]')).isDisplayed().then(function(result) {
                        if ( result ) {

                            var addFilt = element(by.css('[ng-click="applyFilters()"]'));
                            browser.wait(EC.presenceOf(addFilt), 5000);
                            addFilt.click();
                            //browser.pause();
                        } else {
                            //browser.pause();
                        }
                    });
                    element.all(by.repeater('licensee in searchResults.items')).isDisplayed().each(function(element,index) {
                        element.getAttribute('style').then(function (attr) {
                            if(attr!='display: none;')
                            {
                                element.getText().then(function (text) {


                                    console.log(text.split(" "));
                                    var unzip = "//a[contains(text(), '"+text+"')]";
                                    expect(text).toContain(textLic);
                                    //browser.pause();
                                });
                            }
                        });
                    });

                    var cFilters = element(by.css('[ng-click="clearFilters()"]'));
                    browser.wait(EC.presenceOf(cFilters), 15000);
                    cFilters.click();
                    browser.wait(EC.presenceOf(Admin2), 15000);
                    Admin2.click();
                });
            }
            else
            {
                browser.pause();
            }
        });
    });
});

