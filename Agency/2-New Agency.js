describe('New Agency Test', function() {

    beforeAll(function () {
        browser.get("http://trade-qa.acuityads.com:8080//login.html");
        browser.driver.manage().window().maximize();
        expect(browser.getTitle()).toEqual("AcuityAds | Login");
        element(by.name('userName')).clear().sendKeys('Darwin');
        element(by.name('password')).clear().sendKeys('Darwin');
        element(by.className('btn btn-info btn-block aadev-submit ng-binding')).click();
        expect(browser.getTitle()).toEqual("AcuityAds | Programmatic Marketing Platform");
    });

    it('New Agencie - Enter New Agencie Menu', function() {
        var EC = protractor.ExpectedConditions;
        var Admin = element(by.xpath('.//*[text()="Admin"]'));
        browser.wait(EC.visibilityOf(Admin), 15000);
        Admin.click();
        browser.sleep(15000);
        var Agn = element(by.xpath('.//*[text()="Agencies"]'));
        browser.wait(EC.visibilityOf(Agn), 15000);
        Agn.click();
        var NewAgn = element(by.xpath('//*[@id="side-menu"]/li[5]/ul/li[2]/ul/li[3]'));
        browser.wait(EC.visibilityOf(NewAgn), 15000);
        NewAgn.click();
    });

    it('New Agency', function() {
        element(by.name('licenseeId')).click();                  //Licensee
        element(by.xpath('.//*[@label="QA Licensee"]')).click();

        element(by.name('agencyName')).click().sendKeys('TAD_Agency_'+ browser.params.TestSuiteID);
        element(by.name('agencyDisplayName')).click().sendKeys('TAD_Agency_'+ browser.params.TestSuiteID);
        element(by.name('website')).click().sendKeys('www.google.co.ve');

        element(by.name('agencyCampaignManagerId')).click();                  //Campaign Manager
        element(by.xpath('.//*[@label="Acuity  CM"]')).click();

        element(by.name('agencySalesRepId')).click();                         //Sales Rep
        element(By.name('agencySalesRepId')).all(By.xpath('.//option[@label="Acuity Sales"]')).click();

        var addr = element(by.name('agencyAddress1')).sendKeys('181 Bay Street, Toronto, ON M5J 2T3, Canada');                         //Address 181 Bay Street, Toronto, ON M5J 2T3, CA
        addr.click();

        browser.driver.actions().mouseMove(addr);
        addr.sendKeys(protractor.Key.ARROW_DOWN);
        addr.sendKeys(protractor.Key.TAB);

        element(by.name('agencyAddress2')).sendKeys('3333');                          //Suite
        element(by.name('agencyPhone')).sendKeys('55544433322');                      //Phone
        element(by.name('agencyFax')).sendKeys('5554458956');                         //Fax

        element(by.xpath('//*[@id="page-wrapper"]/div[1]/div/div[2]/form/div/div[2]/button')).click();
    });
});
