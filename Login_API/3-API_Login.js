var request = require("superagent");
describe('API - Right Data Login Test Suite', function () {
    it('API - Right Login', function () {

        var data = '{"userName": "AdminQA", "password": "AdminQA"}';

        return request.post('http://trade-qa.acuityads.com:8080/aa/api/v1/auth/login')
                      .set('Content-Type', 'application/json')
                      .send(data).then(function success(resp) {
            console.log(" status = ", resp.status);
            console.log(" authenticationToken = ", resp.body.data.authenticationToken);

            var bool =  (resp.status == 200 &&  resp.body.data.authenticationToken != undefined);
            return expect(true).toEqual(bool);
        }, function error(reason) {
            console.error(" status = ", reason.status);
            console.error(" errors = ", reason.response.body.errors);
            return expect(true).toEqual(reason.response.body.errors);
        });
    });
});