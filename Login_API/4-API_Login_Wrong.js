var request = require("superagent");
describe('API - Wrong Data Login Test Suite', function () {
    it('API - Login - Wrong Password', function () {

        var data = '{"userName": "AdminQA", "password": "sdasdasd"}';

        return request.post('http://trade-qa.acuityads.com:8080/aa/api/v1/auth/login')
                      .set('Content-Type', 'application/json')
                      .send(data).then(function success(resp) {
            console.log(" status = ", resp.status);

            var bool =  (resp.status == 200 &&  resp.body.data.authenticationToken != undefined);
            return expect(false).toEqual(bool);
        }, function error(reason) {
            console.error(" status = ", reason.status);
            console.error(" code = ", reason.response.body.errors[0].code);
            return expect('ERROR.AUTHENTICATION.101').toEqual(reason.response.body.errors[0].code);
        });
    });

    it('API - Login - Wrong UserName', function () {

        var data = '{"userName": "wererwerr", "password": "AdminQA"}';

        return request.post('http://trade-qa.acuityads.com:8080/aa/api/v1/auth/login')
            .set('Content-Type', 'application/json')
            .send(data).then(function success(resp) {
                console.log(" status = ", resp.status);

                var bool =  (resp.status == 200 &&  resp.body.data.authenticationToken != undefined);
                return expect(false).toEqual(bool);
            }, function error(reason) {
                console.error(" status = ", reason.status);
                console.error(" code = ", reason.response.body.errors[0].code);
                return expect('ERROR.AUTHENTICATION.100').toEqual(reason.response.body.errors[0].code);
            });
    });

    it('API - Login - Username/Password', function () {

        var data = '{"userName": "xxxxasdasd23213xxxx", "password": "xxxxx12312312xxxx"}';

        return request.post('http://trade-qa.acuityads.com:8080/aa/api/v1/auth/login')
            .set('Content-Type', 'application/json')
            .send(data).then(function success(resp) {
                console.log(" status = ", resp.status);
                console.log(" authenticationToken = ", resp.body.data.authenticationToken);

                var bool =  (resp.status == 200 &&  resp.body.data.authenticationToken != undefined);
                return expect(false).toEqual(bool);
            }, function error(reason) {
                console.error(" status = ", reason.status);
                console.error(" code = ", reason.response.body.errors[0].code);
                return expect('ERROR.AUTHENTICATION.100').toEqual(reason.response.body.errors[0].code);
            });
    });

});