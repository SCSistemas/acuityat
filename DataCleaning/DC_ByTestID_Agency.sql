set @TestID = (select AgencyID from Agency where Name=@Agn);
select @TestID;
select @Lic;

delete a from UserAdvertiserAccess a
where a.AdvertiserID IN
(
	select AdvertiserID from Advertiser
	inner join Agency on Advertiser.AgencyID = Agency.AgencyID
	where Agency.AgencyID=@TestID
);

delete a from Advertiser a
where a.AdvertiserID IN
(
	select AdvertiserID from
	(
		select AdvertiserID from Advertiser
		inner join Agency on Advertiser.AgencyID = Agency.AgencyID
		where Agency.AgencyID=@TestID
	) as t
);

delete a from UserAgencyAccess a
where a.AgencyID IN
(
	select AgencyID from Agency a
	where a.AgencyID=@TestID
);

delete a from Agency a
where a.AgencyID IN
(
	select AgencyID from
	(
		select a.AgencyID from Agency a
		where a.AgencyID=@TestID
	) as t
);


delete a from Address a
where a.AddressID IN
(
	select AddressID from
	(
		select a.AddressID from Address a
		inner join Agency on a.AddressID = Agency.AddressID
		where Agency.AgencyID=@TestID
	) as t
);


select count(*) as 'Number of delete d Phones Numbers' from Phone Ph
inner join Agency Lic on Ph.PhoneID=Lic.PhoneID
where Lic.AgencyID=@TestID;
delete Ph from Phone Ph
inner join Agency Lic on Ph.PhoneID=Lic.PhoneID
where Lic.AgencyID=@TestID;

select count(*) as 'Number of delete d Fax Numbers' from Phone Fx
inner join Agency Lic on Fx.PhoneID=Lic.FaxID
where Lic.AgencyID=@TestID;
delete Fx from Phone Fx
inner join Agency Lic on Fx.PhoneID=Lic.FaxID
where Lic.AgencyID=@TestID;

select count(*) as 'Number of delete d ' from DealAgencyMapping DlM
where DlM.AgencyID = @TestID;
delete DlM from DealAgencyMapping DlM
where DlM.AgencyID = @TestID;

select count(*) as 'Number of delete d FilterByAdNetworkAgencyMapping' from FilterByAdNetworkAgencyMapping FaNdS
where FaNdS.AgencyID = @TestID;
delete FaNdS from FilterByAdNetworkAgencyMapping FaNdS
where FaNdS.AgencyID = @TestID;

select count(*) as 'Number of delete d FilterByPlacementAgencyMapping' from FilterByPlacementAgencyMapping FpDs
where FpDs.AgencyID = @TestID;
delete FpDs from FilterByPlacementAgencyMapping FpDs
where FpDs.AgencyID = @TestID;

/*select count(*) as 'Number of delete d FilterByPublisherAgencyMapping' from FilterByPublisherAgencyMapping FpubDs
where FpubDs.AgencyID = @TestID;
delete from FilterByPublisherAgencyMapping FpubDs
where FpubDs.AgencyID = @TestID;
*/
select count(*) as 'Number of delete d FilterBySiteAgencyMapping' from FilterBySiteAgencyMapping FSdS
where FSdS.AgencyID = @TestID;
delete FSdS from FilterBySiteAgencyMapping FSdS
where FSdS.AgencyID = @TestID;

select count(*) as 'Number of delete d AgencyAcuitySiteMapping' from AgencyAcuitySiteMapping GaS
where GaS.AgencyID = @TestID;
delete GaS from AgencyAcuitySiteMapping GaS
where GaS.AgencyID = @TestID;

select count(*) as 'Number of delete d Invoice' from Invoice I
where I.AgencyID = @TestID;
delete I from Invoice I
where I.AgencyID = @TestID;

select count(*) as 'Number of delete d AgencyCreditLimitHistory' from AgencyCreditLimitHistory LcLh
where LcLh.AgencyID = @TestID;
delete LcLh from AgencyCreditLimitHistory LcLh
where LcLh.AgencyID = @TestID;

select count(*) as 'Number of delete d AgencyUser' from AgencyUser Lu
where Lu.AgencyID = @TestID;
delete Lu from AgencyUser Lu
where Lu.AgencyID = @TestID;

/*select count(*) as 'Number of delete d PaymentHistory' from PaymentHistory Ph
where Ph.AgencyID = @TestID;
delete Ph from PaymentHistory Ph
where Ph.AgencyID = @TestID;

select count(*) as 'Number of delete d CreditUsage' from CreditUsage Ph
where Ph.AgencyID Ph = @TestID;
delete from CreditUsage Ph
where Ph.AgencyID = @TestID;*/
