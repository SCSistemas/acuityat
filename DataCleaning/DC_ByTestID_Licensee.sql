set @TestID = (select LicenseeID from Licensee b where b.Name=@Lic);
select @TestID;
select @Lic;


delete a from UserAdvertiserAccess a
where a.AdvertiserID IN
(
	select AdvertiserID from Advertiser
	inner join Licensee on Advertiser.LicenseeID = Licensee.LicenseeID
	where Licensee.LicenseeID=@TestID
);

delete a from Advertiser a
where a.AdvertiserID IN
(
	select AdvertiserID from
	(
		select AdvertiserID from Advertiser
		inner join Licensee on Advertiser.LicenseeID = Licensee.LicenseeID
		where Licensee.LicenseeID=@TestID
	) as t
);

/*delete a from UserAgencyAccess a
where a.AgencyID IN
(
	select AgencyID from Agency
	inner join Licensee on Agency.LicenseeID = Licensee.LicenseeID
	where Licensee.LicenseeID=@TestID
);

delete a from Agency a
where a.AgencyID IN
(
	select AgencyID from
	(
		select AgencyID from Agency
		inner join Licensee on Agency.LicenseeID = Licensee.LicenseeID
		where Licensee.LicenseeID=@TestID
	) as t
);*/

delete a from UserLicenseeAccess a
where a.LicenseeID IN
(
	select LicenseeID from Licensee a
	where a.LicenseeID=@TestID
);

select count(*) as 'Number of Deleted Licensees' from Licensee
where LicenseeID=@TestID;
delete a from Licensee a
where a.LicenseeID IN
(
	select LicenseeID from
	(
		select a.LicenseeID from Licensee a
		where a.LicenseeID=@TestID
	) as t
);

delete a from Address a
where a.AddressID IN
(
	select AddressID from
	(
		select a.AddressID from Address a
		inner join Licensee on a.AddressID = Licensee.AddressID
		where Licensee.LicenseeID=@TestID
	) as t
);


/*select count(*) as 'Number of Deleted Addresses' from Address Addr
inner join Licensee Lic on Addr.AddressID=Lic.AddressID
where Lic.LicenseeID=@TestID;
delete Addr from Address Addr
inner join Licensee Lic on Addr.AddressID=Lic.AddressID
where Lic.LicenseeID=@TestID;*/

/*select count(*) as 'Number of Deleted UserLicenseeAccessView' from UserLicenseeAccessView Addr
where Addr.LicenseeID=@TestID;
delete Addr from UserLicenseeAccessView Addr
where Addr.LicenseeID=@TestID;*/

select count(*) as 'Number of Deleted Phones Numbers' from Phone Ph
inner join Licensee Lic on Ph.PhoneID=Lic.PhoneID
where Lic.LicenseeID=@TestID;
delete Ph from Phone Ph
inner join Licensee Lic on Ph.PhoneID=Lic.PhoneID
where Lic.LicenseeID=@TestID;

select count(*) as 'Number of Deleted Fax Numbers' from Phone Fx
inner join Licensee Lic on Fx.PhoneID=Lic.FaxID
where Lic.LicenseeID=@TestID;
delete Fx from Phone Fx
inner join Licensee Lic on Fx.PhoneID=Lic.FaxID
where Lic.LicenseeID=@TestID;

select count(*) as 'Number of Deleted Adnetworks' from AdNetwork AdN
where AdN.LicenseeID = @TestID;
delete AdN from AdNetwork AdN
where AdN.LicenseeID = @TestID;

select count(*) as 'Number of Deleted ' from DealLicenseeMapping DlM
where DlM.LicenseeID = @TestID;
delete DlM from DealLicenseeMapping DlM
where DlM.LicenseeID = @TestID;

select count(*) as 'Number of Deleted FilterByAdNetworkDemandSourceMapping' from FilterByAdNetworkDemandSourceMapping FaNdS
where FaNdS.LicenseeID = @TestID;
delete FaNdS from FilterByAdNetworkDemandSourceMapping FaNdS
where FaNdS.LicenseeID = @TestID;

select count(*) as 'Number of Deleted FilterByPlacementDemandSourceMapping' from FilterByPlacementDemandSourceMapping FpDs
where FpDs.LicenseeID = @TestID;
delete FpDs from FilterByPlacementDemandSourceMapping FpDs
where FpDs.LicenseeID = @TestID;

select count(*) as 'Number of Deleted FilterByPublisherDemandSourceMapping' from FilterByPublisherDemandSourceMapping FpubDs
where FpubDs.LicenseeID = @TestID;
delete FpubDs from FilterByPublisherDemandSourceMapping FpubDs
where FpubDs.LicenseeID = @TestID;

select count(*) as 'Number of Deleted FilterBySiteDemandSourceMapping' from FilterBySiteDemandSourceMapping FSdS
where FSdS.LicenseeID = @TestID;
delete FSdS from FilterBySiteDemandSourceMapping FSdS
where FSdS.LicenseeID = @TestID;

select count(*) as 'Number of Deleted GlobalAcuitySiteMapping' from GlobalAcuitySiteMapping GaS
where GaS.LicenseeID = @TestID;
delete GaS from GlobalAcuitySiteMapping GaS
where GaS.LicenseeID = @TestID;

select count(*) as 'Number of Deleted InsertionOrder' from InsertionOrder Io
where Io.LicenseeID = @TestID;
delete Io from InsertionOrder Io
where Io.LicenseeID = @TestID;

select count(*) as 'Number of Deleted Invoice' from Invoice I
where I.LicenseeID = @TestID;
delete I from Invoice I
where I.LicenseeID = @TestID;

select count(*) as 'Number of Deleted LicenseeBidAlgorithmMapping' from LicenseeBidAlgorithmMapping LbAm
where LbAm.LicenseeID = @TestID;
delete LbAm from LicenseeBidAlgorithmMapping LbAm
where LbAm.LicenseeID = @TestID;

select count(*) as 'Number of Deleted LicenseeCreditLimitHistory' from LicenseeCreditLimitHistory LcLh
where LcLh.LicenseeID = @TestID;
delete LcLh from LicenseeCreditLimitHistory LcLh
where LcLh.LicenseeID = @TestID;

select count(*) as 'Number of Deleted LicenseeDataProviderMapping' from LicenseeDataProviderMapping LdPm
where LdPm.LicenseeID = @TestID;
delete LdPm from LicenseeDataProviderMapping LdPm
where LdPm.LicenseeID = @TestID;

select count(*) as 'Number of Deleted LicenseeFee' from LicenseeFee Lf
where Lf.LicenseeID = @TestID;
delete Lf from LicenseeFee Lf
where Lf.LicenseeID = @TestID;

select count(*) as 'Number of Deleted LicenseeIpListMapping' from LicenseeIpListMapping LiLm
where LiLm.LicenseeID = @TestID;
delete LiLm from LicenseeIpListMapping LiLm
where LiLm.LicenseeID = @TestID;

select count(*) as 'Number of Deleted LicenseeUser' from LicenseeUser Lu
where Lu.LicenseeID = @TestID;
delete Lu from LicenseeUser Lu
where Lu.LicenseeID = @TestID;

select count(*) as 'Number of Deleted PaymentHistory' from PaymentHistory Ph
where Ph.LicenseeID = @TestID;
delete Ph from PaymentHistory Ph
where Ph.LicenseeID = @TestID;

select count(*) as 'Number of Deleted Pixels' from Pixel P
where P.LicenseeID = @TestID;
delete P from Pixel P
where P.LicenseeID = @TestID;

select count(*) as 'Number of Deleted Segments' from Segment Seg
where Seg.LicenseeID = @TestID;
delete Seg from Segment Seg
where Seg.LicenseeID = @TestID;

select count(*) as 'Number of Deleted Topics' from Topic Top
where Top.LicenseeID = @TestID;
delete Top from Topic Top
where Top.LicenseeID = @TestID;

select count(*) as 'Number of Deleted UserDownloadReports' from UserDownloadReport UdR
where UdR.LicenseeID = @TestID;
delete UdR from UserDownloadReport UdR
where UdR.LicenseeID = @TestID;

select count(*) as 'Number of Deleted UserScheduleReports' from UserScheduleReport UsR
where UsR.LicenseeID = @TestID;
delete UsR from UserScheduleReport UsR
where UsR.LicenseeID = @TestID;








