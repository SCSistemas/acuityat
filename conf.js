var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
var csv = require('ya-csv');

exports.config = {
    framework: 'jasmine2',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    //useAllAngular2AppRoots: true,
    //specs: ['Login/*', 'Licensee/*.js'],
    jasmineNodeOpts: {
        showColors: true, // Use colors in the command line report.
        restartBrowserBetweenTests: true,
        defaultTimeoutInterval: 14400000
    },

    capabilities: {
        browserName: 'chrome'
    },

    suites: {
        //Login: './Login/*.js',
        //Licensee: './Licensee/6-List_Campaigns_from_Lic.js'
        //Agency: './Agency/All_Agencies_Filters.js',
        //Advertiser:'./Advertiser/All_Advertisers_Filters.js',
        //LoginApi: './Login_API/*.js',
        /*LicenseeAPI: './Licensee_API/*.js',
        AgencyAPI:'./Agency_API/*.js'*/
        Campaigns:'./Campaigns/1-New Campaign.js',
    },

    params: {
        TestSuiteID: 'someValue',
    },

    onPrepare: function () {
        //global.Functions =  require(__dirname + '/lib/functions/glb_functions.js');

        jasmine.getEnv().addReporter(new Jasmine2HtmlReporter({
            savePath: './test/reports/',
            takeScreenshots: true,
            screenshotsFolder: 'images',
            takeScreenshotsOnlyOnFailures: true
        }));
    }
};
