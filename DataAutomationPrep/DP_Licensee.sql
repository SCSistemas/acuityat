set @UserCreator := (Select UserID from User where Username = 'AdminQA');
select @MaxLicID := (max(LicenseeID)+1) from Licensee;
select @MaxAddrID := (max(AddressID)+1) from Address;
select @MaxPhID := (max(PhoneID)+1) from Phone;
set @MaxFaxID = (@MaxPhID+1);
select @MaxUserID := (max(UserID)+1) from UserLicenseeAccess;

INSERT INTO DemandSource
	(DemandSourceID, SourceType, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
	VALUES (@MaxLicID, 'L', 'A', @UserCreator, NOW(), 0, NOW());
	
INSERT INTO Address
	(AddressID, Address1, Address2, City, RegionCode, CountryCode, PostalCode, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
	VALUES (@MaxAddrID, '181 Bay Street', '222222', 'Toronto', 'ON', 'CA', 'M5J 2T3', 'A', @UserCreator, NOW(), @UserCreator, NOW());

INSERT INTO Phone
	(PhoneID, CountryCode, AreaCode, PhoneNumber, Extension, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
	VALUES (@MaxPhID, '', '', '7777777777', '', 'A', @UserCreator, NOW(), 0, NOW());
	
INSERT INTO Phone
	(PhoneID, CountryCode, AreaCode, PhoneNumber, Extension, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
	VALUES (@MaxFaxID, '', '', '8888888888', '', 'A', @UserCreator, NOW(), 0, NOW());

INSERT INTO Licensee
	(LicenseeID, ParentLicenseeID, Name, DisplayName, AddressID, PhoneID, FaxID, Website, TimeZoneID, CurrencyCode, ThemeID, LogoName, Comments, DefaultFee, AcuityCampaignManagerID, AcuitySalesRepID, Details, Domain, ManageAgency, IsAgency, IsManaged, ManagementCreditCard, TextForLogin, AcuityDefaultOverheadCost, AcuityContractMargin, AcuityLicenseeTechFee, AcuityDefaultCalculationMethod, LicenseeDefaultBillingMethod, LicenseeDefaultMinRate, LicenseeDefaultMaxRate, DefaultLicenseeOverheadCost, LicenseeMargin, LicenseeDefaultMargin, LicenseeDefaultTechFee, DefaultFeedBackLoop, DefaultShowAdChoices, DefaultAutoPlayVideo, TrafficMode, CreditLimit, BudgetSpend, BudgetUpdated, CreditPaid, LicenseeType, RequiresInsertionOrder, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
	VALUES (@MaxLicID, null, 'DP_Licensee', 'DP_Licensee', @MaxAddrID, @MaxPhID, @MaxFaxID, '', 1, 'USD', null, null, '', 0, 154, 155, null, 'http://www.acuityads.com', 0, 0, 0, 0, '', 0.16, 0, 0, '', '', 0, 0, 0, 0, null, 0, 0, 1, 0, 'RTB', 0, 0, NOW(), 0, 'SELF_SERVE', 0, 'A', @UserCreator, NOW(), @UserCreator, NOW());

INSERT INTO UserLicenseeAccess
	(UserID, LicenseeID, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
	VALUES (@UserCreator, @MaxLicID, 'A', @UserCreator, NOW(), 0, NOW())
	
