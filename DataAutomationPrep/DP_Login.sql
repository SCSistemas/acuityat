
/*Buscar el mayor ContactID existente para utilizar el siguiente*/

select @MaxContactID := max(ContactID) from Contact;
set @tmp = now();
select @ID2 := CONCAT('QATestLogin', @tmp);

/*En el siguiente query utilizar, para el primer campo (ContactID), el resultado del anterior +1*/
INSERT INTO Contact
(ContactID,FirstName,LastName,Position,CompanyName,AddressID,PhoneID,FaxID,EmailAddress,Website,Status,CreateBy,CreateDateTime,LastUpdateBy,LastUpdateDateTime)
VALUES (@MaxContactID+1,@ID2,@ID2,@ID2,'AcuityAds',1227,4553,4802,'jesus.urbaez@acuityads.com','','A',null,now(),null,now());

/*Buscar el mayor UserID existente para utilizar el siguiente*/

select @MaxUserID := max(UserID) from User;

/*En el siguiente query utilizar, para el primer campo (UserID), el resultado del anterior +1 y en 7mo campo colocar el ContactID usado anteriormente*/
/*Ademas colocar en el campo ProfileID el tipo de perfil requerido (82 Admin, 84 Licensee, 85 Licensee ReadOnly)*/
INSERT INTO User
(UserID, Username, Password, SecurityQuestion, SecurityAnswer, ProfileID, ContactID, LanguageCode, TimeZoneID, CurrencyCode, LastLogin, LastIP, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
VALUES (@MaxUserID+1, @ID2, @ID2, @ID2, @ID2, 84, @MaxContactID+1, 'en', 1, 'USD', NOW(), '', 'A', 0, NOW(), 0, NOW());


/*Colocar el UserID usado anteriormente en el 1er campo y el ProfileID en el 2do campo)*/
INSERT INTO UserFeatureProfileMapping
(UserID, ProfileID, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
VALUES (@MaxUserID+1, 84, 'A', 412, NOW(), 0, NOW())




