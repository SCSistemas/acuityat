set @UserCreator := (Select UserID from User where Username = 'AdminQA');
select @MaxAgenID := (max(AgencyID)+1) from Agency;
set @LicID = (select LicenseeID from Licensee where Licensee.Name='DP_Licensee');
select @MaxAddrID := (max(AddressID)+1) from Address;
select @MaxPhID := (max(PhoneID)+1) from Phone;
set @MaxFaxID = (@MaxPhID+1);
select @MaxUserID := (max(UserID)+1) from UserAgencyAccess;
	
INSERT INTO Address
	(AddressID, Address1, Address2, City, RegionCode, CountryCode, PostalCode, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
	VALUES (@MaxAddrID, '181 Bay Street', '222222', 'Toronto', 'ON', 'CA', 'M5J 2T3', 'A', @UserCreator, NOW(), @UserCreator, NOW());

INSERT INTO Phone
	(PhoneID, CountryCode, AreaCode, PhoneNumber, Extension, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
	VALUES (@MaxPhID, '', '', '7777777777', '', 'A', @UserCreator, NOW(), 0, NOW());
	
INSERT INTO Phone
	(PhoneID, CountryCode, AreaCode, PhoneNumber, Extension, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
	VALUES (@MaxFaxID, '', '', '8888888888', '', 'A', @UserCreator, NOW(), 0, NOW());
	
INSERT INTO Agency
	(AgencyID, LicenseeID, Name, DisplayName, AgencyName, AddressID, PhoneID, FaxID, Website, TimeZoneID, CurrencyCode, LogoName, Comments, DefaultFee, LicenseeCampaignManagerID, LicenseeSalesRepID, CreditLimit, BudgetSpend, BudgetUpdated, CreditPaid, RebatePercent, RebateDetail, InternalComment, DefaultShowAdChoices, AcuityDefaultOverheadCost, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
	VALUES (@MaxAgenID, @LicID, 'DP_Agency', 'DP_Agency', 'DP_Agency', @MaxAddrID, @MaxPhID,@MaxFaxID, 'http://www.acuityads.com', 1, 'USD', null, '', 0, 155, 155, 0, 0, null, 0, null, null, null, 1, null, 'A', @UserCreator, NOW(), @UserCreator, NOW());
	
INSERT INTO UserAgencyAccess
	(UserID, AgencyID, `Status`, CreateBy, CreateDateTime, LastUpdateBy, LastUpdateDateTime)
	VALUES (@UserCreator, @MaxAgenID, 'A', @UserCreator, NOW(), @UserCreator, NOW());
