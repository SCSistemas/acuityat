describe('Parameters Validation in Licensee Creation Test', function() {

    beforeAll(function () {
        browser.get("http://trade-qa.acuityads.com:8080//login.html");
        browser.driver.manage().window().maximize();
        expect(browser.getTitle()).toEqual("AcuityAds | Login");
        element(by.name('userName')).clear().sendKeys('AdminQA');
        element(by.name('password')).clear().sendKeys('AdminQA');
        element(by.className('btn btn-info btn-block aadev-submit ng-binding')).click();
        expect(browser.getTitle()).toEqual("AcuityAds | Programmatic Marketing Platform");
    });

    afterEach(function() {
        element(by.name('name')).clear();                               //Licensee Name
        element(by.name('displayName')).clear();                        //Display Name
        element(by.name('website')).clear();                            //Website
        element(by.name('address1')).clear();                           //Address 181 Bay Street, Toronto, ON M5J 2T3, CA
        element(by.name('address2')).clear();                           //Suite
        element(by.name('phone')).clear();                              //Phone
        element(by.name('fax')).clear();                                //Fax
    });

    it('New Licensee - Enter New Licensee Menu', function() {
        var EC = protractor.ExpectedConditions;
        var Admin = element(by.xpath('//*[text()="Admin"]'));
        browser.wait(EC.presenceOf(Admin), 15000);
        Admin.click();
        browser.sleep(15000);
        var Lic = element(by.xpath('//*[text()="Licensees"]'));
        browser.wait(EC.visibilityOf(Lic), 15000);
        Lic.click();
        var NewLic = element(by.xpath('//*[@id="side-menu"]/li[5]/ul/li[1]/ul/li[3]'));
        browser.wait(EC.visibilityOf(NewLic), 15000);
        NewLic.click();
    });

    it('New Licensee - Required Field Test', function() {
        element(by.name('name')).click();                               //Licensee Name
        element(by.name('displayName')).click();                        //Display Name
        element(by.name('licenseeTypeId')).click();                     //Type
        element(by.name('website')).click();                            //Website
        element(by.name('licenseeCampaignManagerId')).click();          //Campaign Manager
        element(by.name('licenseeSalesRepId')).click();                 //Sales Rep
        element(by.name('address1')).click();                //Address
        element(by.name('address2')).click();                           //Suite
        element(by.name('phone')).click();                              //Phone
        element(by.name('fax')).click();                                //Fax

        /*---------------------------------------------------------------------------------------------*/

        var requiredFieldMessages = element.all(by.xpath('//span[contains(text(), "This field is required")]'));
        expect(requiredFieldMessages.count()).toEqual(5);
    });

    it('New Licensee - Website Format Test', function() {
        var web = element(by.name('website'));
        web.sendKeys('google');                                         //Website
        var domainFormat = element(by.xpath('//span[contains(text(), "Expected hostname")]'));
        var button = element(by.xpath('//*[@id="page-wrapper"]/div[1]/div/div[2]/form/div/div[2]/button'));
        expect(button.isEnabled()).toBe(true);
    });

    it('New Licensee - Phone Number Lenght Test', function() {
        var phone = element(by.name('phone')).sendKeys('123456789');
        var phoneFormat = element(by.xpath('//span[contains(text(), "This field is too short")]'));
        expect(phoneFormat.getText()).toEqual("This field is too short");
    });

    it('New Licensee - Adress Lenght Test', function() {
        var Addr = element(by.name('address1')).sendKeys('d');
        var Addr = element(by.xpath('//span[contains(text(), "This field is too short")]'));
        expect(Addr.getText()).toEqual("This field is too short");
    });
});