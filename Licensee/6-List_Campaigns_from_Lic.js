'use strict';
var csv = require('ya-csv');

var admin;
var pass;
var filtModal;
var textLic;
var search2;

describe('List Campaigns from Licensee Test', function()
{
    beforeAll(function () {
        browser.get("http://trade-qa.acuityads.com:8080//login.html");
        browser.driver.manage().window().maximize();
        expect(browser.getTitle()).toEqual("AcuityAds | Login");
        element(by.name('userName')).clear().sendKeys('Darwin');
        element(by.name('password')).clear().sendKeys('Darwin');
        element(by.className('btn btn-info btn-block aadev-submit ng-binding')).click();
        expect(browser.getTitle()).toEqual("AcuityAds | Programmatic Marketing Platform");
    });

    it('Filters - Enter All Licensee Menu', function() {
        var EC = protractor.ExpectedConditions;
        var Admin = element(by.xpath('//*[text()="Admin"]'));
        browser.wait(EC.presenceOf(Admin), 5000);
        Admin.click();
        browser.sleep(5000);
        var Lic = element(by.xpath('//*[text()="Licensees"]'));
        browser.wait(EC.visibilityOf(Lic), 5000);
        Lic.click();
        var NewLic = element(by.xpath('//*[@id="side-menu"]/li[5]/ul/li[1]/ul/li[1]'));
        browser.wait(EC.visibilityOf(NewLic), 5000);
        NewLic.click();
    });

    it('Check Filters Availability', function() {
        var EC = protractor.ExpectedConditions;
        var Admin2 = element(by.xpath('//Button[contains(text(), "Add a filter")]'));
        browser.wait(EC.presenceOf(Admin2), 5000);
        var filt = element.all(by.repeater('option in aSearchOptions'));
        expect(filt.count()).toEqual(7);
    });

    it('Check Filters Apply', function() {
        var EC = protractor.ExpectedConditions;
        var Admin2 = element(by.xpath('//Button[contains(text(), "Add a filter")]'));
        browser.wait(EC.presenceOf(Admin2), 15000);
        Admin2.click();
        var filt = element.all(by.repeater('option in aSearchOptions'));

        element.all(by.repeater('option in aSearchOptions')).isDisplayed().then(function(result) {
            if ( result )
            {
                var FiltLic = element(by.xpath('//*[@id="filters"]/div[1]/div/div/ul/li[1]/a[2]'));
                FiltLic.click();
                filtModal = element(by.css('[ng-click="$select.activate()"]'));
                filtModal.click();
                filtModal.sendKeys('qa li'); //search2
                browser.driver.actions().mouseMove(filtModal);
                filtModal.sendKeys(protractor.Key.ARROW_DOWN);
                filtModal.sendKeys(protractor.Key.TAB);
                filtModal.getText().then(function (text2)
                {
                    textLic = text2;
                });
            }
            else
            {
                browser.pause();
            }
        });
        element(by.css('[ng-click="applyFilters()"]')).isDisplayed().then(function(result) {
            if ( result ) {

                var addFilt2 = element(by.css('[ng-click="addFilters()"]'));
                //browser.wait(EC.presenceOf(addFilt2), 5000);
                addFilt2.click();
                //browser.pause();
            } else {
                //browser.pause();
            }
        });
        element(by.css('[ng-click="applyFilters()"]')).isDisplayed().then(function(result) {
            if ( result ) {

                var addFilt = element(by.css('[ng-click="applyFilters()"]'));
                browser.wait(EC.presenceOf(addFilt), 5000);
                addFilt.click();
                //browser.pause();
            } else {
                //browser.pause();
            }
        });
        var el = element.all(by.repeater('licensee in searchResults.items')).get(0);
        el.element(by.xpath('//*[@id="page-wrapper"]/div/div/div[2]/div[4]/table/tbody/tr/td[3]/a')).click();

        element(by.xpath('//*[@id="page-wrapper"]/div/div/div[3]/div/ul/li[5]/a')).click();

        /*element.all(by.repeater("licensee in searchResults.items")).isDisplayed().filter(function (elm) {
         return elm.getText().then(function (text) {
         return text == "DP_Licensee";
         });
         }).then(function(filteredElements) {
         filteredElements[0].click();
         });*/
        /*var cFilters = element(by.css('[ng-click="clearFilters()"]'));
         browser.wait(EC.presenceOf(cFilters), 15000);
         cFilters.click();
         browser.wait(EC.presenceOf(Admin2), 15000);
         Admin2.click();*/
        //browser.sleep(25000);

        element.all(by.repeater('campaign in aCampaignSearchResults.items')).isDisplayed().then(function(el,index)
        {
            var results =  element.all(by.repeater('campaign in aCampaignSearchResults.items'));
            element.all(by.repeater('campaign in aCampaignSearchResults.items')).then(function(count) {
                var a = results.length;
                expect(a).toEqual(185);
            });
            //console.log(results.count());

            //browser.pause();

            /*var index2 = 1;
             var mierda = '//*[@id="agencies"]/div/div/table/tbody/tr['+index2+']/td[2]/a';
             element(by.xpath(mierda)).click();
             console.log(mierda);*/
            /*var EC = protractor.ExpectedConditions;
             var licenseeName = element(by.name('licenseeId'));
             browser.wait(EC.presenceOf(Admin2), 5000);*/
            /*element(by.name('licenseeId')).getText().then(function(text){
             console.log(text); //print the text that output element holds
             expect(text).toEqual("QA Licensee");
             });
             browser.waitForAngular().then(function() {
             browser.navigate().back();
             //expect(browser.getTitle()).toMatch('page title');
             //expect(browser.getCurrentUrl()).toContain("/flight/los-angeles/");
             });
             //browser.pause(7000);
             /*element(by.name('licenseeId')).isDisplayed().then(function(result) {
             //expect(text).toContain("QA Licensee");
             //browser.pause();
             expect(result.getText()).toEqual("QA Licensee");
             browser.navigate().back();
             });*/
        });
    });
    /*it('Check Listing', function() {

     element(by.css('[ng-click="clearFilters()"]')).isDisplayed().then(function (result) {
     element(by.css('[ng-click="clearFilters()"]')).click();
     });

     element(by.css('[ng-click="applyFilters()"]')).isDisplayed().then(function(result) {
     if ( result ) {
     element(by.css('[ng-click="applyFilters()"]')).click();
     //browser.pause();
     } else {
     //browser.pause();
     }
     });

     element(by.repeater('licensee in searchResults.items')).isDisplayed().then(function(result) {
     if ( result ) {
     var mierda2 = element(by.repeater('licensee in searchResults.items').row(1).column('licenseeId'));
     console.log('text',mierda2.getText(0));
     browser.pause();
     } else {
     browser.pause();
     }
     });

     element.all(by.repeater('licensee in searchResults.items')).isDisplayed().then(function(result) {
     if ( result ) {
     var mierda = element.all(by.repeater('licensee in searchResults.items'));
     console.log('text',mierda.getText(0));
     browser.pause();
     } else {
     browser.pause();
     }
     });

     });*/
});


