describe('Licensee Tests', function() {

    it('New Licensee', function() {
        element(by.name('name')).sendKeys('TAD_Licensee_'+ browser.params.TestSuiteID);                            //Licensee Name
        element(by.name('displayName')).sendKeys('TAD_Licensee_'+ browser.params.TestSuiteID);                      //Display Name

        element(by.name('licenseeTypeId')).click();                             //Type
        element(by.xpath('.//*[@label="Self Serve"]')).click();

        element(by.name('website')).sendKeys('www.google.co.ve');               //Website

        element(by.name('licenseeCampaignManagerId')).click();                  //Campaign Manager
        element(by.xpath('.//*[@label="Acuity  CM"]')).click();

        element(by.name('licenseeSalesRepId')).click();                         //Sales Rep
        element(by.xpath('.//option[@label="Acuity Sales"]')).click();

        var addr = element(by.name('address1')).sendKeys('181 Bay Street, Toronto, ON M5J 2T3, Canada');                         //Address 181 Bay Street, Toronto, ON M5J 2T3, CA
        addr.click();
        browser.sleep(15000);
        browser.driver.actions().mouseMove(addr);
        addr.sendKeys(protractor.Key.ARROW_DOWN);
        addr.sendKeys(protractor.Key.TAB);

        element(by.name('address2')).sendKeys('3333');                          //Suite
        element(by.name('phone')).sendKeys('55544433322');                      //Phone
        element(by.name('fax')).sendKeys('5554458956');                         //Fax

        element(by.xpath('//*[@id="page-wrapper"]/div[1]/div/div[2]/form/div/div[2]/button')).click();
    });
});